import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  create_api(data) {
    return this.http.post(environment.baseUrl + `/emp/create_api`, data);
  }

  update_api(data, id) {
    return this.http.put(environment.baseUrl + `/emp/update_api?id=` + id, data);
  }

  delete_api(id) {
    return this.http.delete(environment.baseUrl + `/emp/delete_api?id=` + id);
  }
  get_data() {
    return this.http.get(environment.baseUrl + `/emp/get_data`);
  }





}