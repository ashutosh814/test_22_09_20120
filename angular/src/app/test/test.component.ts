import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from '../service/http.service';
import { ExcelService } from '../service/excel.service'
declare var $: any

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  get_emp_list = []
  displayedColumns: string[] = ['emp_id', 'name', 'address', 'address1', 'qualification', 'earnings', 'deduction', 'totalpay', 'action'];
  dataSource = new MatTableDataSource(this.get_emp_list);
  submitted: boolean = false
  addData: FormGroup


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private fb: FormBuilder,
    private api: HttpService,
    private router: Router,
    private excelService: ExcelService
  ) { }
  Name: String; Qualification: String; Total_pay: Number;
  Deduction: Number; Earnings: Number; Address1: String; Address: String;
  get f1() { return this.addData.controls; }
  ngOnInit() {
    this.get_all_data();
    this.addData = this.fb.group({
      name: ['', Validators.required],
      Address: ['', Validators.required],
      Address1: [''],
      Earnings: ['', Validators.required],
      Deduction: ['', Validators.required],
      Total_pay: ['', Validators.required],
      Qualification: ['', Validators.required],
    }
    )
  }

  get_all_data() {
    this.api.get_data().subscribe(
      data => {
        this.get_emp_list = data['response'];
        this.dataSource = new MatTableDataSource(this.get_emp_list);
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
        }, 500);
      },
      error => {
        alert(error)
      }
    );

  }

  save_value() {
    debugger
    this.submitted = true
    if (this.addData.invalid) return
    let data_to_send = {
      Name: this.addData.value.name,
      Address: this.addData.value.Address,
      Address1: this.addData.value.Address1,
      Qualification: this.addData.value.Qualification,
      Total_pay: this.addData.value.Total_pay,
      Earnings: this.addData.value.Earnings,
      Deduction: this.addData.value.Deduction
    }
    this.api.create_api(data_to_send).subscribe(
      data => {
        $('#add').modal('hide')
        this.get_emp_list.push(data['response'])
        this.dataSource = new MatTableDataSource(this.get_emp_list);
        this.valueBlank()
      },
      error => {
        alert(error.error.message);
      }
    );
  }
  calculate() {
    this.addData.value.Earnings
    if (this.addData && this.addData.value && this.addData.value.Earnings && this.addData.value.Deduction) {
      let value = this.addData.value.Earnings - this.addData.value.Deduction
      this.addData.patchValue({
        Total_pay: value ? value : 0
      })
    }
  }
  valueBlank() {
    this.addData.patchValue({
      Total_pay: 0,
      name: '',
      Address: '',
      Address1: '',
      Qualification: '',
      Earnings: '',
      Deduction: ''
    })
    this.submitted = false
  }
  edit: any = false
  editDataVal: any
  index: any
  editData(val, index) {
    this.edit = true
    this.editDataVal = val
    this.index = index
    this.addData.patchValue({
      name: val.Name,
      Address: val.Address,
      Address1: val.Address1,
      Qualification: val.Qualification,
      Earnings: val.Earnings,
      Deduction: val.Deduction,
      Total_pay: (val.Earnings - val.Deduction)
    })
    $('#add').modal('show')
  }
  openAdd() {
    this.edit = false
    $('#add').modal('show')
  }
  update_value() {
    this.submitted = true
    if (this.addData.invalid) return
    let data_to_send = {
      Name: this.addData.value.name,
      Address: this.addData.value.Address,
      Address1: this.addData.value.Address1,
      Qualification: this.addData.value.Qualification,
      Total_pay: this.addData.value.Total_pay,
      Earnings: this.addData.value.Earnings,
      Deduction: this.addData.value.Deduction
    }
    this.api.update_api(data_to_send, this.editDataVal._id).subscribe(
      data => {
        $('#add').modal('hide')
        this.edit = false
        this.get_emp_list[this.index] = data['response']
        this.dataSource = new MatTableDataSource(this.get_emp_list);
        this.valueBlank()
      },
      error => {
        alert(error.error.message);
      }
    );
  }
  
  deleteConfirm(val, index) {
    this.api.delete_api(val._id).subscribe(
      data => {
        $('#delete-modal').modal('hide')
        this.get_emp_list.splice(index,1)
        this.dataSource = new MatTableDataSource(this.get_emp_list);
      },
      error => {
        alert(error.error.message);
      }
    );
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }





  exports(): void {
    this.excelService.exportAsExcelFile(this.get_emp_list, 'empList');
  }
}

