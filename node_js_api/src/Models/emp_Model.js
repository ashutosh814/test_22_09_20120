const { ObjectID } = require('mongodb');
const { mongoose, conn } = require('../Modules/connection');

let EmpSchema = new mongoose.Schema({

    Name: {
        type: String,
        unique:true
    },
    Address: {
        type: String
    },
    Address1: {
        type: String,
        default:true
    },
    Earnings:{
        type:Number,
        default:0
    },
    Deduction: {
        type: Number,
        default: 0
    },
    Total_pay:{
        type:Number,
        default:0
    },
    Qualification:{
        type:String
    }

}, {
    versionKey: false,
    collection: 'Employee'
});
exports.EmployeeModel = conn.model('Employee', EmpSchema);
