var http = require ("http");
var express = require ("express");
var cors = require ("cors");
var path = require ("path");
var glob = require("glob");
var bodyParser = require ("body-parser");

const app = express();
app.server = http.createServer(app);

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());
app.use(cors());

const responseHandler = require('./Middlewares/responseHandler')
app.use('/', responseHandler);
let initRoutes = () => {
	// including all routes
	glob("./Routes/*.js", {cwd: path.resolve("./src")}, (err, routes) => {
		if (err) {
			console.log("Error occured including routes");
			return;
		}
		routes.forEach((routePath) => {
			require(routePath).getRouter(app); // eslint-disable-line
		});
		//cron.reminder();
		console.log("included " + routes.length + " route files");
	});
}
initRoutes(app);
const port = process.env.PORT || 3000;

app.server.listen(port);
console.log("Started on port " + port);