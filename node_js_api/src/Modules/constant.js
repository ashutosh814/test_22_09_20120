let define = (obj, name, value) => {
    Object.defineProperty(obj, name, {
        value: value,
        enumerable: true,
        writable: false,
        configurable: true
    });
}

exports.responseFlags = {};
exports.responseMessages = {};

//FOR MESSAGES
define(exports.responseMessages, 'PARAMETER_MISSING',                     'Some parameter missing.');
define(exports.responseMessages, 'INVALID_ACCESS_TOKEN',                  'Invalid access token.');
define(exports.responseMessages, 'SHOW_ERROR_MESSAGE',                    'Show error message.');
define(exports.responseMessages, 'ERROR_IN_EXECUTION',                    'Error in execution.');
define(exports.responseMessages, 'UPLOAD_ERROR',                          'Error in uploading.');
define(exports.responseMessages, 'STATUS_CHANGED_SUCCESSFULLY',           'Status changed successfully.');
define(exports.responseMessages, 'USER_NOT_FOUND',                        'User not found.');
define(exports.responseMessages, 'NO_DATA_FOUND',                         'No data found.');
define(exports.responseMessages, 'EMAIL_ALREADY_EXISTS',                  'Email already registered');
define(exports.responseMessages, 'MOBILE_ALREADY_EXISTS',                 'Mobile already registered');
define(exports.responseMessages, 'INVALID_PIN' ,						  'INVALID_PIN' );

//FOR FLAGS
define(exports.responseFlags, 'PARAMETER_MISSING',                   422);
define(exports.responseFlags, 'INVALID_ACCESS_TOKEN',                401);
define(exports.responseFlags, 'STATUS_CHANGED_SUCCESSFULLY',         200);
define(exports.responseFlags, 'NO_DATA_FOUND',                       204);