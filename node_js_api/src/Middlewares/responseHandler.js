module.exports = (req, res, next) => {
    res.ashuSuccess = function(result, message = '') {
        if(message == '') {
            this.send({response : result});
        } else {
            this.send({ message, response : result});
        }
    }

    res.ashuError = function(error) {
        this.send({ message : error});
    }
    
    next();
}