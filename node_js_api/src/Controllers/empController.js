var commFunc = require ('../Modules/commonFunction');
var responses = require ('../Modules/responses');
var constant = require ('../Modules/constant');
var config = require ('../Config/production')
var async = require ('async');
var uniqid = require ('uniqid');
var { EmployeeModel} = require('../Models/emp_Model')


exports.create_api = async(req,res)=>{
    try {
        let {Name, Address, Address1, Earnings, Deduction, Total_pay, Qualification} = req.body;
        let check_name = await EmployeeModel.findOne({ Name},{},{lean:true})
        if(check_name){
            throw new Error("Name is already present")
        }
        let data_to_save = { Name, Address, Address1, Earnings, Deduction, Total_pay, Qualification}
        let emplyee = new EmployeeModel(data_to_save);
        let empDetails = await emplyee.save();
        if (!empDetails) {
            throw new Error('Unable to add')
        }
        res.ashuSuccess(empDetails, 'Successfully created')        
    } catch (error) {
        res.status(403).ashuError(error.message)
    }
}

exports.update_api = async(req,res)=>{
    try {
        let _id = req.query.id;
        let data = req.body;
        let update_value = await EmployeeModel.findByIdAndUpdate({_id},{$set:data},{new:true})
        res.ashuSuccess(update_value, 'Successfully updated') 
    } catch (error) {
        res.status(403).ashuError(error.message)
    }
}

exports.delete_api = async (req, res) => {
    try {
        let _id = req.query.id;
        let update_value = await EmployeeModel.findByIdAndDelete({ _id })
        res.ashuSuccess({}, 'Successfully')
    } catch (error) {
        res.status(403).ashuError(error.message)
    }
}

exports.get_data = async (req, res) => {
    try {
        let update_value = await EmployeeModel.find({ },{},{lean:true})
        res.ashuSuccess(update_value, 'Successfully')
    } catch (error) {
        res.status(403).ashuError(error.message)
    }
}


