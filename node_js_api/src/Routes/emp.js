var emp = require ('../Controllers/empController.js');
var express = require  ('express')

exports.getRouter = (app) => {

    app.route("/emp/create_api").post(emp.create_api)
    app.route("/emp/update_api").put(emp.update_api)
    app.route("/emp/delete_api").delete(emp.delete_api)
    app.route("/emp/get_data").get(emp.get_data)
    

}